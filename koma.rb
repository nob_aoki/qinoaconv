#!/usr/local/bin/ruby -w
# coding: utf-8

# csa format specification
# http://www.computer-shogi.org/wcsc12/record.html
# http://www.computer-shogi.org/protocol/record_v22.html

#　FU　…　歩　　　TO　…　成歩(と金)
#　KY　…　香車　　NY　…　成香
#　KE　…　桂馬　　NK　…　成桂
#　GI　…　銀　　　NG　…　成銀
#　KI　…　金
#　KA　…　角　　　UM　…　成角(馬)
#　HI　…　飛車　　RY　…　成飛車(龍)
#　OU　…　王将


#
# 駒一般。駒無しも表す。
#
class Koma
  # 先手駒
  SENTE = 0
  # 後手駒
  GOTE = 1
  # 駒無し
  AKI = 2

  # 成ってない
  NARAZU = 0
  # 成った
  NARI = 1

  # 初期化
  #
  # teban :: SENTE, GOTE, AKI
  def initialize(teban = AKI)
    @str = "* "
    @strnari = "* "
    @bnari = NARAZU
    @teban = teban
  end

  # 駒を成る
  def nari
    @bnari = NARI
  end

  # 駒を取る
  def toru
    if @teban == SENTE
      @teban = GOTE
    elsif @teban == GOTE
      @teban = SENTE
    end
    @bnari = NARAZU;
  end

  # 駒の状態
  #
  # ex. "FU" or "TO"
  def stat
    if (@bnari == NARAZU)
      return @str
    else
      if @strnari == nil
        print "@strnari is nil!!\n"
        print "@str:#{@str}\n"
        print "@bnari:#{@bnari}\n"
        print "@teban:#{@teban}\n"
      end
      return @strnari
    end
  end

  # 先手後手の情報も含めて出力
  #
  # ex. "+RY" 先手の竜
  # ex. "-OU" 後手の玉
  # ex. " * " 駒無し
  def put
    if (@teban == SENTE)
      print "+"
    elsif (@teban == GOTE)
      print "-"
    else 
      print " "
    end
    print stat
  end

  # 同じ駒かチェック
  def ==(rhs)
    if (rhs.class == nil.class)
      print "rhs ==nil"
      print stat
      print ":"
      print rhs.class
      print "\n"
      return 0
    end
    return @str == rhs.str
  end

  # 手番(先手, 後手)
  attr_reader:teban
  # 駒の種類(不成り時)
  attr_reader:str
  #attr_reader:strnari
end

# 歩
class Fu < Koma
  def initialize(teban = SENTE)
    @str = "FU"
    @strnari = "TO"
    @bnari = 0
    @teban = teban
  end
end

# 香車
class Kyosha < Koma
  def initialize(teban = SENTE)
    @str = "KY"
    @strnari = "NY"
    @bnari = 0
    @teban = teban
  end
end

# 桂馬
class Keima < Koma
  def initialize(teban = SENTE)
    @str = "KE"
    @strnari = "NK"
    @bnari = 0
    @teban = teban
  end
end

# 銀将
class Gin < Koma
  def initialize(teban = SENTE)
    @str = "GI"
    @strnari = "NG"
    @bnari = 0
    @teban = teban
  end
end

# 金将
class Kin < Koma
  def initialize(teban = SENTE)
    @str = "KI"
    @strnari = "KI"
    @bnari = 1
    @teban = teban
  end
end

# 飛車
class Hisha < Koma
  def initialize(teban = SENTE)
    @str = "HI"
    @strnari = "RY"
    @bnari = 0
    @teban = teban
  end
end

# 角行
class Kaku < Koma
  def initialize(teban = SENTE)
    @str = "KA"
    @strnari = "UM"
    @bnari = 0
    @teban = teban
  end
end

# 玉将
class Gyoku < Koma
  def initialize(teban = SENTE)
    @str = "OU"
    @strnari = "OU"
    @bnari = 0
    @teban = teban
  end
end
