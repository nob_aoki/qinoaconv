#!/usr/local/bin/ruby -w
# coding: utf-8
#
#Authors::   ぢるっち
#Version::   1.0
#Copyright:: Copyright (C) 2014. All rights reserved.
#License::   NYSLライセンス
#
#=サンプル
#rec	g20140404082028-39e35a6d
#add datetime	2014-04-04 08:20:28
#update datetime	2014-04-04 08:23:59
#state	done.win.b
#tag	[ja]
#sente	kukkiee.doll
#gote	gest
#move count=59
#title	no title
#kihu	
#k1=s2014-04-04 08:20:28
#k1.moto=77
#k1.toko=76
#k1.time=1
#k1.aa=0
#k2=u2014-04-04 08:20:31
#k2.moto=33
#k2.toko=34
#k2.time=1
#
# 先手が駒台にある角を６六に打った。
#k9.moto=113
# 先手が駒台にある歩を７四に打った
#k31.moto=118
# 先手が６二に銀を打った
#k43.moto=115
# 先手が６三に金を打った
#k49.moto=114
# 先手が４三に桂を打った
#k55.moto=116
# 後手が７四に歩を打った
#k42.moto=126
#

require "./koma.rb"
require "./sashite.rb"
require "./ban.rb"

#
# きのあ形式を読み込んで解釈するクラス
#
class QinoaReader
  # 初期化
  def initialize()
    @teme = 0
    @sashite = []  # 指し手のリストを保持する
    @comment = ""
  end

  # 読み込んだ指し手のリスト
  attr_reader :sashite

  # 1行読み込み
  #
  # buf :: 1行分の文字列。改行文字ダメ。
  # taikyoku :: Taikyokuクラス。
  def readline(buf, taikyoku)
    #buf.chop!
    if (buf =~ /^kihu/)
    elsif (buf =~ /^sente/)
      key, value = buf.split(/\t/, 2)
      taikyoku.sente_name = value
    elsif (buf =~ /^gote/)
      key, value = buf.split(/\t/, 2)
      taikyoku.gote_name = value
    elsif (buf =~ /^state/)
    #elsif (buf =~ /^tag/)
    elsif (buf =~ /^title/)
      key, value = buf.split(/\t/, 2)
      taikyoku.event_name = value
    #elsif (buf =~ /^rec/)
    elsif (buf =~ /^add datetime/)
      key, value = buf.split(/\t/, 2)
      taikyoku.hajime_time = value
    #elsif (buf =~ /^update datetime/)
    #elsif (buf =~ /^move count/)
    else
      if (buf =~ /^k/)
        # kから始まっている
        key, value = buf.split(/=/, 2)
        keynum, keyitem = key.split(/\./)
        #print "key, keynum, keyitem, value:#{key}, #{keynum}, #{keyitem}, #{value}\n"
        if (keyitem == "moto")
          # ex. k2.moto=33
          if (value.length == 2)
            # 
            @xm = value.to_i/10
            @ym = value.to_i%10
            @koma = nil
          elsif (value.length == 3)
            # 打った
            @xm = 0
            @ym = 0
            komaidx = value.to_i-110
            teban = Koma::SENTE
            if (komaidx > 8)
              komaidx = komaidx-8
              teban = Koma::GOTE
            end
            #print "komaidx:#{komaidx}\n"
            case komaidx
            #when 1 then  # 玉?
            #  @koma = Gyoku.new(teban)
            when 2 then  # 飛?
              @koma = Hisha.new(teban)
            when 3 then  # 角
              @koma = Kaku.new(teban)
            when 4 then  # 金
              @koma = Kin.new(teban)
            when 5 then  # 銀
              @koma = Gin.new(teban)
            when 6 then  # 桂
              @koma = Keima.new(teban)
            when 7 then  # 香?
              @koma = Kyosha.new(teban)
            when 8 then  # 歩
              @koma = Fu.new(teban)
            #when 9 then
            else         # エラー
              @koma = Koma.new(teban)
            end
          elsif (value.length == 1)
            # 終了
            @xm = -1
            @ym = -1
          else
            # error
          end
        elsif (keyitem == "toko")
          # ex. k2.toko=34
          if (value.length == 2)
            # 
            @xs = value.to_i/10
            @ys = value.to_i%10
            @nari = 0
          elsif (value.length == 3)
            # 成った
            @xs = value.to_i/10 - 10
            @ys = value.to_i%10
            @nari = 1
          else
            # error
          end
        elsif (keyitem == "time")
          # ex. k2.time=1
          @byou = value
          if (@xm <0 || @ym < 0)
          else
            @sashite.push(Sashite.new(@xm, @ym, @xs, @ys, @byou, @nari, @koma))
          end
        elsif (keyitem == nil || keyitem.length == 0)
          # ex. k1=s2014-04-04 08:20:28
          
        else
          # k10.aa=100 とか
        end
      else
        # k以外から始まっている
        # コメントとして入れておく
        @comment += "'#{buf}\n"
      end
    end
  end
end

=begin
taikyoku = Taikyoku.new("はぶ", "もりうち", "名人戦第１局", "2014/04/08 14:30:00")
ban = Ban.new

qr = QinoaReader.new

while (1)
  buf = STDIN.gets
  if (buf == nil || buf.length < 1)
    break
  end
  buf.chop!
  qr.readline(buf, taikyoku)
end
#qr.readline("sente	kukkiee.doll", taikyoku)
#qr.readline("gote	gest", taikyoku)

taikyoku.putCSAHeader(ban)
#ban.put
#ban.tegoma

#print "\n"

count = false
qr.sashite.each do |sst|
  if (count == false)
    ban.sente(sst)
  else
    ban.gote(sst)
  end
#=begin
ban.put
ban.tegoma
#=end
  count = !count
end

#=begin
ban.sente(Sashite.new(7, 7, 7, 6, 1))
ban.gote(Sashite.new(3, 3, 3, 4, 2))
ban.sente(Sashite.new(8, 8, 2, 2, 3, 1))
ban.gote(Sashite.new(3, 1, 2, 2, 4))
ban.sente(Sashite.new(7, 9, 8, 8, 5))
ban.gote(Sashite.new(0, 0, 5, 5, 6, 0, Kaku.new(Koma::GOTE)))
ban.sente(Sashite.new(8, 8, 7, 7, 7))

print "\n"

ban.put
ban.tegoma
#=end
=end
