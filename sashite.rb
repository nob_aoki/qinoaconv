#!/usr/local/bin/ruby -w
# coding: utf-8

# csa format specification
# http://www.computer-shogi.org/wcsc12/record.html
# http://www.computer-shogi.org/protocol/record_v22.html

#　FU　…　歩　　　TO　…　成歩(と金)
#　KY　…　香車　　NY　…　成香
#　KE　…　桂馬　　NK　…　成桂
#　GI　…　銀　　　NG　…　成銀
#　KI　…　金
#　KA　…　角　　　UM　…　成角(馬)
#　HI　…　飛車　　RY　…　成飛車(龍)
#　OU　…　王将

require './koma.rb'

class Sashite
  XSIZE = 9
  YSIZE = 9
  def initialize(xmoto, ymoto, xsaki, ysaki, sec, nari = 0, uchi = nil)
    if (xmoto == 0 && ymoto == 0)
      if (uchi.class == nil.class)
        print "sashite_error_uchi:#{xmoto},#{ymoto},#{uchi}\n"
      end
    else
      if (xmoto < 1 || xmoto > XSIZE)
        print "sashite_error_xmoto:#{xmoto}\n"
      end
      if (ymoto < 1 || ymoto > YSIZE)
        print "sashite_error_ymoto:#{ymoto}\n"
      end
    end
    if (xsaki < 1 || xsaki > XSIZE)
      print "sashite_error_xsaki:#{xsaki}\n"
    end
    if (ysaki < 1 || ysaki > YSIZE)
      print "sashite_error_ysaki:#{ysaki}\n"
    end

    @xmoto = xmoto
    @ymoto = ymoto
    @xsaki = xsaki
    @ysaki = ysaki

    @sec = sec

    @nari = nari

    @koma = uchi
  end

  def setkoma (koma)
    @koma = koma
  end

  def putCSA
    if @koma.teban == Koma::SENTE
      print "+#{@xmoto}#{@ymoto}#{@xsaki}#{@ysaki}"+@koma.stat()+",T#{@sec}\n"
    else
      print "-#{@xmoto}#{@ymoto}#{@xsaki}#{@ysaki}"+@koma.stat()+",T#{@sec}\n"
    end
  end
  attr_reader:xmoto
  attr_reader:ymoto
  attr_reader:xsaki
  attr_reader:ysaki
  attr_reader:sec
  attr_reader:nari
  attr_reader:koma
end
