#!d:\ruby193\bin\ruby
# -*- encoding: utf-8 -*-

#!/usr/bin/ruby

require "cgi"

require "./qinoareader.rb"

# ウインドウタイトル
$pagetitle = %Q{きのあ形式コンバーター}

# ページタイトル
$titlename = %{きのあ形式コンバーター}

#
# きのあコンバーターCGI本体
#
class QinoaConv
  def initialize(cgi)
    @cgi = cgi;
    @params = cgi.params;
#    if @params.length>0
#      @params.each_value{|val|
#        val.gsub!(',','&#44;');
#      p val
#    }
#    end
    @action = cgi.query_string
  end
  # class methods

  #
  # cgi実行本体。
  # QUERY_STRINGによる分岐
  #
  def perform
    if @action == "CSA"
      csa_cvt_screen
    else
      input_screen
    end
  end

  #
  # CSA形式に変換
  #
  def csa_cvt_screen
    @qna = @params["qna"][0].encode(:universal_newline => true)
    @qna2 = @params["qna2"][0].encode(:universal_newline => true)

    taikyoku = Taikyoku.new("はぶ", "もりうち", "名人戦第１局", "0000/00/00 00:00:00")
    ban = Ban.new
    qr = QinoaReader.new

    @qna.sub!(/\r\n/n){|buf| "\n"}
    @qna2.sub!(/\r\n/n){|buf| "\n"}

    @kihu = @qna + @qna2

    @qna.each_line{|buf|
      buf.chop!
      qr.readline(buf, taikyoku)
    }

    QinoaConv::HTMLHead($pagetitle)
    QinoaConv::HTMLmenu($titlename)

    QinoaConv::HTMLCSA_begin()

    taikyoku.putCSAHeader(ban)
    count = false
    qr.sashite.each do |sst|
      if (count == false)
        ban.sente(sst)
      else
        ban.gote(sst)
      end
      count = !count
    end
    taikyoku.putCSAFooter()

    QinoaConv::HTMLCSA_end()

    QinoaConv::HTMLinputqna(@qna, @qna2)

    QinoaConv::HTMLfoot()
  end

  #
  # デフォルトの入力画面
  #
  def input_screen
    QinoaConv::HTMLHead($pagetitle)
    QinoaConv::HTMLmenu($titlename)
    @params.each_value{|val|
      p val
    }

    QinoaConv::HTMLinputqna(@qna, @qna2)
    #QinoaConv::HTMLhelp();
# testing
=begin
    QinoaConv::test();
    QinoaConv::testCGI(@params);
    #print "@action=",@action,"<BR>\n";
=end
# testing
    QinoaConv::HTMLfoot()
  end

  # class methods

  # HTMLヘッダ出力
  def self.HTMLHead(title)
    print "Content-Type: text/html\n\n",
      "<HTML lang=ja>\n<HEAD>\n <TITLE>",title,"</TITLE>\n",
      " <META http-equiv='Content-Type' content='text/html; charset=utf-8' >\n",
      #"<link rel='stylesheet' type='text/css' href='qinoacnv.css'>\n",
      " <STYLE type=text/css>\n<!--\n",
      "-->\n </STYLE>\n",
      " <SCRIPT type='text/javascript'>\n",
      "<!--\n\nfunction show_help_text()\n{\n",
      "  document.all.help_text.style.display='block';\n",
      "  document.all.btn_help_text_show.style.display='none';\n",
      "  document.all.btn_help_text_hide.style.display='inline';\n}\n\n",
      "function hide_help_text()\n{\n",
      "  document.all.help_text.style.display='none';\n",
      "  document.all.btn_help_text_show.style.display='inline';\n",
      "  document.all.btn_help_text_hide.style.display='none';\n}\n",
      "-->\n </SCRIPT>\n</HEAD>\n<BODY>\n";
  end

  # メニュー部分の出力
  def self.HTMLmenu(title)
    print "<div align='center'>",title,"</div><HR/>\n";
=begin
    print "<div align='center' class='menubase'>&nbsp;",
      "<a class='menu' href='qinoacnv.rb'> 検索 </a>&nbsp;",
      "<a class='menu' href='qinoacnv.rb?add'> 追加 </a>&nbsp;",
      "<a class='menu' href=\"javascript:window.open('qinoacnv.rb?tag','TAG','statusbar=yes,height=400,width=400');\"> タグ一覧 </a>&nbsp;",
      "</div><HR/>\n";
=end
  end

  # フッターの出力
  def self.HTMLfoot
    print "<HR/><div align=right>&copy;ぢるっち 2014</div></BODY></HTML>";
  end

  # きのあ形式入力用フォームの出力
  #
  # text :: 内容がエディットボックスに表示される。
  # text2 :: 内容がエディットボックスに表示される。
  def self.HTMLinputqna(text, text2)
    print "<form action='qinoacnv.rb?CSA' method='POST'>\n",
          "<TABLE align='center'><TR><TH colspan=2>\n",
          "きのあ形式</TH></TR><TR><TD>",
          "<TEXTAREA name='qna' rows='20' cols='40'>#{text}</TEXTAREA></TD><TD>\n",
          "<TEXTAREA name='qna2' rows='20' cols='40'>#{text2}</TEXTAREA>\n",
          "</TD></TR><TR><TD>rec～kifuまでをココにコピー。</TD>\n",
          "<TD>テキストボックスの中身をココにコピー</TD></TR>",
          "<TR><TD><input type='reset' value='リセット'></TD><TD align='right'>",
          "<input type='submit' class='sbmt' value='  to CSA  '>",
          "</TD></TR></TABLE></form>\n"
  end

  # CSA形式出力欄始まり
  def self.HTMLCSA_begin
    print "<TABLE align='center'><TR><TH>CSA形式</TH></TR>\n",
          "<TR><TD><TEXTAREA name='csa' rows='40' cols='40'>"
  end

  # CSA形式出力欄閉じ
  def self.HTMLCSA_end
    print "</TEXTAREA></TD></TR></TABLE>\n<HR>\n"
  end
end

# -----------------------------------
#   main
#

cgi = CGI.new;
qnacnv = QinoaConv.new(cgi);
qnacnv.perform();

# -----------------------------------
#   testing
#
