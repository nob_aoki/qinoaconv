#!/usr/local/bin/ruby -w
# coding: utf-8

# csa format specification
# http://www.computer-shogi.org/wcsc12/record.html
# http://www.computer-shogi.org/protocol/record_v22.html

#　FU　…　歩　　　TO　…　成歩(と金)
#　KY　…　香車　　NY　…　成香
#　KE　…　桂馬　　NK　…　成桂
#　GI　…　銀　　　NG　…　成銀
#　KI　…　金
#　KA　…　角　　　UM　…　成角(馬)
#　HI　…　飛車　　RY　…　成飛車(龍)
#　OU　…　王将

require './koma.rb'
require './sashite.rb'


# CSA形式出力盤
class Ban
  XSIZE = 9
  YSIZE = 9
  NARI_BORDER_SENTE = 3
  NARI_BORDER_GOTE = 9-3
  def initialize
    @komas = [
      [Kyosha.new(Koma::GOTE), Keima.new(Koma::GOTE), Gin.new(Koma::GOTE), Kin.new(Koma::GOTE), Gyoku.new(Koma::GOTE), Kin.new(Koma::GOTE), Gin.new(Koma::GOTE), Keima.new(Koma::GOTE), Kyosha.new(Koma::GOTE)],
      [Koma.new, Hisha.new(Koma::GOTE), Koma.new, Koma.new, Koma.new, Koma.new, Koma.new, Kaku.new(Koma::GOTE), Koma.new],
      [Fu.new(Koma::GOTE), Fu.new(Koma::GOTE), Fu.new(Koma::GOTE), Fu.new(Koma::GOTE), Fu.new(Koma::GOTE), Fu.new(Koma::GOTE), Fu.new(Koma::GOTE), Fu.new(Koma::GOTE), Fu.new(Koma::GOTE)],
      [Koma.new, Koma.new, Koma.new, Koma.new, Koma.new, Koma.new, Koma.new, Koma.new, Koma.new],
      [Koma.new, Koma.new, Koma.new, Koma.new, Koma.new, Koma.new, Koma.new, Koma.new, Koma.new],
      [Koma.new, Koma.new, Koma.new, Koma.new, Koma.new, Koma.new, Koma.new, Koma.new, Koma.new],
      [Fu.new(Koma::SENTE), Fu.new(Koma::SENTE), Fu.new(Koma::SENTE), Fu.new(Koma::SENTE), Fu.new(Koma::SENTE), Fu.new(Koma::SENTE), Fu.new(Koma::SENTE), Fu.new(Koma::SENTE), Fu.new(Koma::SENTE)],
      [Koma.new, Kaku.new(Koma::SENTE), Koma.new, Koma.new, Koma.new, Koma.new, Koma.new, Hisha.new(Koma::SENTE), Koma.new],
      [Kyosha.new(Koma::SENTE), Keima.new(Koma::SENTE), Gin.new(Koma::SENTE), Kin.new(Koma::SENTE), Gyoku.new(Koma::SENTE), Kin.new(Koma::SENTE), Gin.new(Koma::SENTE), Keima.new(Koma::SENTE), Kyosha.new(Koma::SENTE)]
      ]
    @sentegoma = [Koma.new]
    @gotegoma = [Koma.new]
  end

  def sente(sashite)
    xmoto = XSIZE-sashite.xmoto
    ymoto = sashite.ymoto - 1

    xsaki = XSIZE-sashite.xsaki
    ysaki = sashite.ysaki - 1

    if (sashite.koma.class == nil.class)
      komamoto = @komas[ymoto][xmoto]
      if (komamoto.class == nil.class)
        print "komamoto = @komas[ymoto][xmoto] => nil!!\n"
        print "sente_error_mototeban:(#{xmoto},#{ymoto})\n"
        print "koma:#{komamoto.class}\n"
        print "sashite:#{sashite.xmoto},#{sashite.ymoto}"
        sashite.putCSA
        print "--> BAN ERROR\n"
        put
      end
      sashite.setkoma(komamoto)
      if (komamoto.teban != Koma::SENTE)
        print "sente_error_mototeban:(#{xmoto},#{ymoto})\n"
        print "koma:"
        komamoto.put
        print "\n"
        print "sashite:#{sashite.xmoto},#{sashite.ymoto}"
        #sashite.putCSA
        print "--> BAN ERROR\n"
        put
      end
      @komas[ymoto][xmoto] = Koma.new
    else
      komamoto = sashite.koma
      #komamoto.put
      #print "uchikomaclass:"+sashite.koma.class
      idx = @sentegoma.find_index(komamoto)
      if (idx == nil)
        print "sente_error_tegoma_utenai:"
        komamoto.put
        print "\n"
        tegoma
      else
        @sentegoma.delete_at idx
      end
    end

    komasaki = @komas[ysaki][xsaki]
    if (komasaki.teban == Koma::SENTE)
      print "sente_error_sakiteban:(#{xmoto},#{ymoto})\n"
      print "koma:"
      komasaki.put
      print "\n"
    end  

    if (sashite.nari == 1)
      komamoto.nari
    end
    @komas[ysaki][xsaki] = komamoto

    if (komasaki.teban == Koma::AKI)
    end
    if (komasaki.teban == Koma::GOTE)
      komasaki.toru
      @sentegoma.push(komasaki)
    end

    sashite.putCSA
  end

  def gote(sashite)
    xmoto = XSIZE-sashite.xmoto
    ymoto = sashite.ymoto - 1

    xsaki = XSIZE-sashite.xsaki
    ysaki = sashite.ysaki - 1

    if (sashite.koma.class == nil.class)
      komamoto = @komas[ymoto][xmoto]
      if (komamoto.class == nil.class)
        print "komamoto = @komas[ymoto][xmoto] => nil!!\n"
        print "gote_error_mototeban:(#{xmoto},#{ymoto})\n"
        print "koma:#{komamoto.class}\n"
        print "sashite:#{sashite.xmoto},#{sashite.ymoto}"
        sashite.putCSA
        print "--> BAN ERROR\n"
        put
      end
      sashite.setkoma(komamoto)
      if (komamoto.teban != Koma::GOTE)
        print "gote_error_mototeban:(#{xmoto},#{ymoto})\n"
        print "koma:"
        komamoto.put
        print "\n"
        print "sashite:#{sashite.xmoto},#{sashite.ymoto}"
        #sashite.putCSA
        print "--> BAN ERROR\n"
        put
      end
      @komas[ymoto][xmoto] = Koma.new
    else
      komamoto = sashite.koma
      #komamoto.put
      idx = @gotegoma.find_index(komamoto)
      if (idx == nil)
        print "gote_error_tegoma_utenai:"
        komamoto.put
        print "\n"
        tegoma
      else
        @gotegoma.delete_at idx
      end
    end

    komasaki = @komas[ysaki][xsaki]
    if (komasaki.teban == Koma::GOTE)
      print "gote_error_sakiteban:(#{xmoto},#{ymoto})\n"
      print "koma:"
      komasaki.put
      print "\n"
    end

    if (sashite.nari == 1)
      komamoto.nari
    end
    @komas[ysaki][xsaki] = komamoto

    if (komasaki.teban == Koma::AKI)
    end
    if (komasaki.teban == Koma::SENTE)
      komasaki.toru
      @gotegoma.push(komasaki)
    end

    sashite.putCSA
  end


  def put
    idx = 1
    @komas.each do |ary|
      print "P#{idx}"
      ary.each do |a|
        a.put
      end
      print "\n"
      idx+=1
    end
  end
  def tegoma
    print "sente_tegoma:"
    @sentegoma.each do |tegoma|
      tegoma.put
      print ","
    end

    print "\ngote_tegoma:"
    @gotegoma.each do |tegoma|
      tegoma.put
      print ","
    end
    print "\n"
  end

end

class Taikyoku
  def initialize (sente_name = "", gote_name = "", event_name = "", hajime_time = "")
    @sente_name = sente_name
    @gote_name = gote_name
    @event_name = event_name
    @hajime_time = hajime_time
  end

  def putCSAHeader(ban)
    print "'encoding=Shift_JIS\n"
    print "' ---- CSA形式棋譜ファイル ----\n" #.encode('cp932')
    print "V2.2\n"
    print "N+#{@sente_name}\n" #.encode('cp932')
    print "N-#{@gote_name}\n" #.encode('cp932')
    print "$EVENT:#{@event_name}\n" #.encode('cp932')
    print "$START_TIME:#{@hajime_time}\n" #.encode('cp932')
    print "PI\n" #.encode('cp932') #平手はPI # ban.put
    putsenteban
  end

  def putsenteban
    print "+\n"
  end

  def putgoteban
    print "-\n"
  end

  def putCSAFooter
    print "%TORYO\n"
  end

  attr_accessor :sente_name, :gote_name, :event_name, :hajime_time
end

=begin
taikyoku = Taikyoku.new("はぶ", "もりうち", "名人戦第１局", "2014/04/08 14:30:00")
ban = Ban.new

taikyoku.putCSAHeader(ban)
#ban.put
#ban.tegoma

#print "\n"

ban.sente(Sashite.new(7, 7, 7, 6, 1))
ban.gote(Sashite.new(3, 3, 3, 4, 2))
ban.sente(Sashite.new(8, 8, 2, 2, 3, 1))
ban.gote(Sashite.new(3, 1, 2, 2, 4))
ban.sente(Sashite.new(7, 9, 8, 8, 5))
ban.gote(Sashite.new(0, 0, 5, 5, 6, 0, Kaku.new(Koma::GOTE)))
ban.sente(Sashite.new(8, 8, 7, 7, 7))

print "\n"

ban.put
ban.tegoma
=end
